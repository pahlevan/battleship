﻿using BattleShip.Core.Entities.Board;
using BattleShip.Core.Utils;
using BattleShip.Core.Enums;
using Xunit;
using BattleShip.Core.Entities.Ship;

namespace BattleShip.Test
{
    public class BoardTest
    {

        [Theory]
        [InlineData(10, 10,-1,0,false, BoardCellStatus.OutOfBound)]
        [InlineData(10, 10, 0, -1, false, BoardCellStatus.OutOfBound)]
        [InlineData(10, 10, 11, 0, false, BoardCellStatus.OutOfBound)]
        [InlineData(10, 10, 0, 11, false, BoardCellStatus.OutOfBound)]
        [InlineData(10, 10, 0,1,true, BoardCellStatus.Water)]
        [InlineData(10, 10, 9, 9,true, BoardCellStatus.Water)]
        public void GridBoard_test_CreatBoard(int rowNum, int colNum, int row, int col, bool validity, BoardCellStatus status)
        {
            //Create new board
            Board board = new GridBoard(rowNum, colNum);
            Coordinate coordinate = new Coordinate(row, col);

            //Check Validity
            Assert.True(board.GetCoordinateValidity(coordinate) == validity);

            //Check status
            Assert.True(board.GetCellStatus(coordinate) == status);
  
        }


        [Theory]
        [InlineData(10, 10, 0, 0, BoardCellStatus.UnDamagedShipPiece, 1)]
        [InlineData(10, 10, 0, 0, BoardCellStatus.DamagedShipPiece, 5)]
        public void GridBoard_test_UpdateCell(int rowNum, int colNum, int row, int col, BoardCellStatus status, int shipId)
        {
            //Arrange
            Board board = new GridBoard(rowNum, colNum);
            Coordinate coordinate = new Coordinate(row, col);

            //Act
            board.UpdateCellStatus(coordinate, status, shipId);

            //Assert
            if( board.GetCoordinateValidity(coordinate))
                Assert.True(board.GetCellStatus(coordinate) == status);
            else
                Assert.True(board.GetCellStatus(coordinate) == BoardCellStatus.Water);

            Assert.True(board.GetShipId(coordinate) == shipId);
       
        }



        [Theory]
        [InlineData(10, 10, 0, 0, ShipOrientation.Horizental,3, true)]
        [InlineData(10, 10, 0, 0, ShipOrientation.Vertical,3, true)]
        [InlineData(10, 10, 9, 0, ShipOrientation.Vertical, 3, false)]
        [InlineData(10, 10, 0, 8, ShipOrientation.Horizental, 3, false)]
        public void GridBoard_test_IsShipPlaceMentPossible(int rowNum, int colNum, int row, int col, ShipOrientation orientation, int totalPieceNum, bool validity)
        {
            //Arrange
            Board board = new GridBoard(rowNum, colNum);
            Coordinate coordinate = new Coordinate(row, col);
            Ship ship = new Ship(0, coordinate,orientation, totalPieceNum);

            // check validity
            Assert.True(board.IsShipPlaceMentPossible(ship) == validity);

        }

        [Theory]
        [InlineData(10, 10, 0, 0, 5, ShipOrientation.Horizental, 3)]
        public void GridBoard_test_AddShip(int rowNum, int colNum, int row, int col, int id, ShipOrientation orientation, int totalPieceNum)
        {
            //Arrange
            Board board = new GridBoard(rowNum, colNum);
            Coordinate coordinate = new Coordinate(row, col);
            Ship ship = new Ship(id, coordinate, orientation, totalPieceNum);


            //Add ship
            board.AddShip(ship);

            //Assert
            for (int i = 0; i < totalPieceNum; i++)
            {
                Assert.True(board.GetCellStatus(coordinate) == BoardCellStatus.UnDamagedShipPiece);
                if (orientation == ShipOrientation.Vertical)
                    coordinate.IncreaseRow();
                else
                    coordinate.IncreaseCol();
            }

        }

        [Theory]
        [InlineData(BoardType.Grid,10, 10, -1, 0, false, BoardCellStatus.OutOfBound)]
        [InlineData(BoardType.Grid, 10, 10, 0, -1, false, BoardCellStatus.OutOfBound)]
        [InlineData(BoardType.Grid, 10, 10, 11, 0, false, BoardCellStatus.OutOfBound)]
        [InlineData(BoardType.Grid, 10, 10, 0, 11, false, BoardCellStatus.OutOfBound)]
        [InlineData(BoardType.Grid, 10, 10, 0, 1, true, BoardCellStatus.Water)]
        [InlineData(BoardType.Grid, 10, 10, 9, 9, true, BoardCellStatus.Water)]
        public void GridBoard_test_BoardCreator(BoardType type, int rowNum, int colNum, int row, int col, bool validity, BoardCellStatus status)
        {
            BoardCreator boardCreator = new BoardCreator();
            Board board = boardCreator.CreateBoard(type, rowNum, colNum);
            Coordinate coordinate = new Coordinate(row, col);

            //Check Validity
            Assert.True(board.GetCoordinateValidity(coordinate) == validity);

            //Check status
            Assert.True(board.GetCellStatus(coordinate) == status);
        }

    }
}
