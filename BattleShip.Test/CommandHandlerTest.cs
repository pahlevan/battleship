﻿using System;
using BattleShip.Core.Controllers;
using BattleShip.Core.Controllers.CommandHandlers;
using BattleShip.Core.Controllers.Commands;
using BattleShip.Core.Enums;
using Xunit;

namespace BattleShip.Test
{
    public class CommandHandlerTest
    {

        [Theory]
        [InlineData("/addship", "/addship row column orientation size", "/addship 0 0 v 3")]
        public void CreateAddShipCommand_test(string keyWord, String description, String commandStr)
        {
            //Arrange
            Command command = new AddShipCommand(keyWord, 4, description);


            //Check Validity
            Assert.True(command.Descirption == description);
            Assert.True(command.IsValid(commandStr.Split(" ")));

        }

        [Theory]
        [InlineData("/attack", "/attack row column", "/attack 0 0")]
        public void CreateAttackCommand_test(string keyWord, String description, String commandStr)
        {
            //Arrange
            Command command = new AttackCommand(keyWord, 2, description);


            //Check Validity
            Assert.True(command.Descirption == description);
            Assert.True(command.IsValid(commandStr.Split(" ")));

        }


        [Theory]
        [InlineData("/menu", "/menu", "/menu")]
        public void CreateSingleCommand_test(string keyWord, String description, String commandStr)
        {
            //Arrange
            Command command = new SingleCommand(keyWord, 0, description, CommandType.Menu);


            //Check Validity
            Assert.True(command.Descirption == description);
            Assert.True(command.IsValid(commandStr.Split(" ")));

        }

        [Theory]
        [InlineData("/attack 0 0", CommandType.Attack)]
        [InlineData("/addship 0 0 v 3", CommandType.AddShip)]
        [InlineData("/abc", CommandType.Invalid)]

        public void CreateCommandHandler_test(String command, CommandType type)
        {
            //Arrange
            ConsoleCommandHandler handler = new ConsoleCommandHandler();
            Assert.True(handler.GetCommand(command).Type == type);

        }



    }
}
