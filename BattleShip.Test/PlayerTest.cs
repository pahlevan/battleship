﻿using BattleShip.Core.Entities;
using BattleShip.Core.Entities.Board;
using BattleShip.Core.Entities.Ship;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;
using Xunit;

namespace BattleShip.Test
{
    public class PlayerTest
    {


        [Theory]
        [InlineData(10, 10, Core.Enums.BoardType.Grid)]
        public void CreatePlayer_test(int rowNum, int colNum, Core.Enums.BoardType type)
        {
            //Arrange
            BoardCreator boardCreator = new BoardCreator();
            Board board = boardCreator.CreateBoard(type, rowNum, colNum);
            Player player = new Player(board);


            //Check Validity
            Assert.True(player.GetNewShipId() == 0);
            Assert.True(player.IsShipIdValid(0) == false);
            Assert.True(player.Status == Core.Enums.PlayerStatus.Playing);

            player.UpdatePlayerStatus();
            Assert.True(player.Status == Core.Enums.PlayerStatus.Playing);


        }

        [Theory]
        [InlineData(10, 10, Core.Enums.BoardType.Grid, 1, 0, 0, ShipOrientation.Vertical, 3)]
        [InlineData(10, 10, Core.Enums.BoardType.Grid, 1, 0, 0, ShipOrientation.Horizental, 3)]
        [InlineData(10, 10, Core.Enums.BoardType.Grid, 1, 1, 5, ShipOrientation.Vertical, 4)]

        public void CreatePlayer_AddShip(int rowNum, int colNum, Core.Enums.BoardType type, int id, int row, int col, ShipOrientation orientation, int totalPieceNum)
        {
            //Arrange
            BoardCreator boardCreator = new BoardCreator();
            Board board = boardCreator.CreateBoard(type, rowNum, colNum);
            Player player = new Player(board);
            Coordinate coordinate = new Coordinate(row, col);

            Ship ship = new Ship(id, coordinate, orientation, totalPieceNum);

            //Add ship to player's board
            player.AddShip(ship);

            //Assert
            for (int i = 0; i < totalPieceNum; i++)
            {
                Assert.True(board.GetCellStatus(coordinate) == BoardCellStatus.UnDamagedShipPiece);
                if (orientation == ShipOrientation.Vertical)
                    coordinate.IncreaseRow();
                else
                    coordinate.IncreaseCol();
            }

        }


        [Theory]
        [InlineData(10, 10, Core.Enums.BoardType.Grid, 1, 1)]
        [InlineData(10, 10, Core.Enums.BoardType.Grid, 6, 2)]

        public void CreatePlayer_UnderAttack(int rowNum, int colNum, Core.Enums.BoardType type, int row, int col)
        {
            //Arrange
            BoardCreator boardCreator = new BoardCreator();
            Board board = boardCreator.CreateBoard(type, rowNum, colNum);
            Player player = new Player(board);
            Coordinate coordinate = new Coordinate(row, col);

            // check cell status
            Assert.True(board.GetCellStatus(coordinate) == BoardCellStatus.Water);


            //Add ship to player's board
            int totalPieceNum = 3;
            Ship ship = new Ship(-1, coordinate, ShipOrientation.Horizental, totalPieceNum);
            player.AddShip(ship);



            //Assert
            for (int i = 0; i < totalPieceNum-1; i++)
            {
                Assert.True(player.UnderAttack(coordinate) == CommandResult.AttackHit);
                coordinate.IncreaseCol();
            }
            Assert.True(player.UnderAttack(coordinate) == CommandResult.AttackSunk);
            Assert.True(player.Status == PlayerStatus.Lost);

        }


    }
}
