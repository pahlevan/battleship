using BattleShip.Core.Entities.Ship;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;
using Xunit;

namespace BattleShip.Test
{
    public class ShipTest
    {
        [Theory]
        [InlineData(1, 0, 0, ShipOrientation.Vertical, 3)]
        public void CreateShip_test(int id, int row, int col, ShipOrientation orientation, int totalPieceNum)
        {
            // create coordinate
            Coordinate coordinate = new Coordinate(row, col);

            // create ship
            Ship ship = new Ship(id, coordinate, orientation, totalPieceNum);

            // check health status before and after damage
            Assert.True(ship.Health == totalPieceNum);
            for(int i=1;i<= totalPieceNum;i++)
            {
                ship.Damage();
                Assert.True(ship.Health == totalPieceNum-i);
            }
            Assert.True(ship.IsSunk() == true);

        }

    }
}
