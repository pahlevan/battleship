﻿using System;
using System.Collections.Generic;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers.CommandHandlers
{
    public abstract class CommandHandler
    {
        public List<Command> CommandList { get; protected set; }

        public abstract void ShowMessage(string msg);
        public abstract void ShowCommandList();
        public abstract Command GetUserCommand();
        public abstract List<Command> CreateCommandList();
        public abstract void ShowCommandResult(CommandResult result);
        
    }
}
