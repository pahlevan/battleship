﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using BattleShip.Core.Controllers.CommandHandlers;
using BattleShip.Core.Controllers.Commands;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers
{
		
    public class ConsoleCommandHandler : CommandHandler
    {

        public ConsoleCommandHandler()
        {
            CommandList = CreateCommandList();
        }


        // Create command list
        public override List<Command> CreateCommandList()
        {
            List<Command> commands = new List<Command>();
            commands.Add(new AddShipCommand("/addship", 4, "/addship row column orientation size \n *row(int) *column(int) *orientation(v= vertical, h= horizental) *size(int) "));
            commands.Add(new AttackCommand("/attack", 2, "/attack row column \n *row(int) *column(int)"));
            commands.Add(new SingleCommand("/start", 0, "/start", CommandType.Start));
            commands.Add(new SingleCommand("/status", 0, "/status", CommandType.Status));
            commands.Add(new SingleCommand("/menu", 0, "/menu", CommandType.Menu));
            commands.Add(new SingleCommand("/quit", 0, "/quit", CommandType.Quit));

            return commands;
        }

        //Show message in Console
        public override void ShowMessage(String msg)
        {
            Console.WriteLine(msg);
        }

        public override Command GetUserCommand()
        {
            String consoleCommand = Console.ReadLine();
            return GetCommand(consoleCommand);
        }

        public Command GetCommand(String consoleCommand)
        {
            consoleCommand = Regex.Replace(consoleCommand, @"\s+", " ");
            String[] consoleCommandParts = consoleCommand.Split(' ');

            foreach (Command command in CommandList)
            {
                if (command.IsValid(consoleCommandParts))
                {
                    command.SetArguments(consoleCommandParts);
                    return command;
                }
            }
            return new SingleCommand("Invalid", 0, "Unfortunately this command is Invalid, please check the syntax", CommandType.Invalid);
        }


        public override void ShowCommandList()
        {
            Console.WriteLine("------------");
            foreach (Command command in CommandList)
                Console.WriteLine(command.Descirption);
            Console.WriteLine("------------");
        }
		
		// Show the result of command execution to the user.
        public override void ShowCommandResult(CommandResult result)
        {
			
            switch (result)
            {
                case CommandResult.AddShipDone:
                    ShowMessage("Ship Successfully Added!");
                    break;
                case CommandResult.AddShipFaild:
                    ShowMessage("This Ship could not be placed at this coordinate with the input size");
                    break;
                case CommandResult.AttackFaild:
                    ShowMessage("The Attack Coordinate is not valid.");
                    break;
                case CommandResult.AttackRepeated:
                    ShowMessage("Already Hit!");
                    break;
                case CommandResult.AttackHit:
                    ShowMessage("Hit!");
                    break;
                case CommandResult.AttackMiss:
                    ShowMessage("Miss!");
                    break;
                case CommandResult.AttackSunk:
                    ShowMessage("Hit and Sunk!");
                    break;

            }
        }

       
    }
}
