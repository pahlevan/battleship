﻿using System;
using BattleShip.Core.Entities;
using BattleShip.Core.Entities.Ship;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Controllers.Commands
{
    public class AddShipCommand : Command
    {

        private Ship Ship;
        public AddShipCommand(string keyWord, int argNum, String description) : base(keyWord, argNum, description) {
            Type = CommandType.AddShip;
        }

        public override bool IsValidArguments(string[] command)
        {

                if (!int.TryParse(command[1], out int r))
                return false;

                if (!int.TryParse(command[2], out int c))
                    return false;


                switch (command[3].ToLower())
                {
                    case "v":
                        break;
                    case "h":
                        break;
                    default:
                        return false;
                }

            if (!int.TryParse(command[4], out int s) || Convert.ToInt32(command[4])<0)
                return false;

            return true;
            }


        public override void SetArguments(string[] command)
        {
            int r = Convert.ToInt32(command[1]);
            int c = Convert.ToInt32(command[2]);
            Coordinate headCoordinate = new Utils.Coordinate(r, c);
            ShipOrientation orientation = command[3].ToLower() == "v" ? ShipOrientation.Vertical : ShipOrientation.Horizental;
            int totalPieceNum = Convert.ToInt32(command[4]);
            Ship = new Ship(-1, headCoordinate, orientation, totalPieceNum);
        }

        public override CommandResult Execute(Player player)
        {
            return player.AddShip(Ship);
          
        }
    }
    
}
