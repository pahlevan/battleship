﻿using System;
using BattleShip.Core.Entities;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Controllers.Commands
{
    public class AttackCommand : Command
    {

        private Coordinate coordinate;
        public AttackCommand(string keyWord, int argNum, String description) : base(keyWord, argNum, description) {
            Type = CommandType.Attack;
        }



        public override bool IsValidArguments(string[] command)
        {
            if (!int.TryParse(command[1], out int r))
                return false;

            if (!int.TryParse(command[2], out int c))
                return false;

            return true;

        }

        public override void SetArguments(string[] command)
        {
            int r = Convert.ToInt32(command[1]);
            int c = Convert.ToInt32(command[2]);

            coordinate = new Coordinate(r, c);
        }

        public override CommandResult Execute(Player player)
        {
            return player.UnderAttack(coordinate);
        }

    }
}
