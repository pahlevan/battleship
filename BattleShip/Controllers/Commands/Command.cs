﻿using System;
using System.Collections.Generic;
using BattleShip.Core.Entities;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers
{
    public abstract class Command
    {
        private String keyWord;
        private int argNum;
        public CommandType Type { get; protected set; }
        public String Descirption { get;  set; }

        public Command(String keyWord, int argNum, String descirption)
        {
            this.keyWord = keyWord;
            this.argNum = argNum;
            this.Descirption = descirption;
        }


        public bool IsValid(String[] command)
        {
            if (command.Length == 0 || command[0].ToLower() != keyWord)
                return false;
            if (command.Length - 1 != argNum)
                return false;
            if (!IsValidArguments(command))
                return false;
            return true;
        }

        public abstract void SetArguments(string[] command);

        public abstract bool IsValidArguments(String[] command);

        public abstract CommandResult Execute(Player player);

    }
}
