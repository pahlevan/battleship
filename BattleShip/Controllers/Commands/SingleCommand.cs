﻿using System;
using BattleShip.Core.Entities;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers.Commands
{
    public class SingleCommand : Command
    {
        public SingleCommand(string keyWord, int argNum, String description, CommandType type) : base(keyWord, argNum, description) {
            Type = type;
        }

        public override bool IsValidArguments(string[] command)
        {
            return true;
        }

        public override void SetArguments(string[] command)
        {
            return;
        }

        public override CommandResult Execute(Player player)
        {
            return CommandResult.None;
        }
    }
}
