﻿using System;
using BattleShip.Core.Controllers.CommandHandlers;
using BattleShip.Core.Entities;
using BattleShip.Core.Entities.Board;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers
{
    /*
     * This class is Single Player Controller which use ConsoleCommandHandler that communicate with player using Console commands
     */
    public class ConsoleSinglePlayerController : Controller
    {
        private Player Player;

        public ConsoleSinglePlayerController()
        {
            this.Player = CreatePlayer();
            this.CommandHandler = new ConsoleCommandHandler();  // Communicate with user  via command line.

        }

        /* 
		*	Creating the board game.  
		*	The board creation could be based on the size of the board, for example, we would be able to exploit possible sparsity 
		*	if the size of the board increases.
		*	Currently the Grid type (a 2D matrix) was used based on the information provided as the board is 10 X 10.
		*/
        public Player CreatePlayer()
        {
            BoardCreator boardCreator = new BoardCreator();
            Board board = boardCreator.CreateBoard(Enums.BoardType.Grid, 10, 10);
		
            Player player = new Player(board);
            return player;
        }

        public override void EndGame()
        {
            this.GameStatus = GameStatus.Finished;
            CommandHandler.ShowMessage("Thank you for playing BattleShip Game with us!");
        }


        public override void ShowMenu()
        {

            CommandHandler.ShowCommandList();
        }
		
		// Play executes the user commands and makes actions based on that. 
        public override void Play()
        {
			// Receive and handle user's command based on command handler type.
            Command command = CommandHandler.GetUserCommand();

            switch (command.Type)
            {
				// Check whether the input command is valid or not.
                case CommandType.Invalid:
                    CommandHandler.ShowMessage(command.Descirption);
                    ShowMenu();
                    return;
				
				// Start the game to finalize the board and be able to perform an attack.
                case CommandType.Start:
                    if (!IsGameStarted())
                    {
                        this.GameStatus = GameStatus.Started;
                        CommandHandler.ShowMessage("The game started Succesfully.");
                    }
                    else
                        CommandHandler.ShowMessage("The game has already been started.");
                    return;

                // Show player status
                case CommandType.Status:
                    CommandHandler.ShowMessage(Player.GetStatusInfo());
                    return;

                // Show help menu
                case CommandType.Menu:
                    ShowMenu();
                    return;
				
				// End the game and close the command window
                case CommandType.Quit:
                    EndGame();
                    return;
				
				// Addship to the board
                case CommandType.AddShip:
					
					// After starting the game, no more ships could be added!
                    if (IsGameStarted())
                    {
                        CommandHandler.ShowMessage("You can not add Ship after the game is started!");
                        return;
                    }
                    break;
					
				// Performing an attack!
                case CommandType.Attack:
                    
					// Before performing an attack you should start the game.
					if (!IsGameStarted())
                    {
                        CommandHandler.ShowMessage("Please start the game before sending attack command.");
                        return;
                    }
                    break;

            }

            CommandResult result = command.Execute(Player);
            CommandHandler.ShowCommandResult(result);

            Player.Board.ShowBoard();
            if (Player.Status == PlayerStatus.Lost)
            {
                CommandHandler.ShowMessage("No more Ship is available, all the ships have been sunk.");
                EndGame();
            }
        }
    }
}
