﻿using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers
{
    public class ContollerCreator
    {

        /*
         * This class should be used for creating a controller, please specify the type in the "controllerType" enum 
         * and define the controller class by inheriting from "Controller.cs" file 
         */
        public Controller CreateController(ControllerType controllerType)
        {

            switch (controllerType)
            {
                case ControllerType.ConsoleSinglePlayer:
                    return new ConsoleSinglePlayerController();
                case ControllerType.ConsoleMultiPlayer:
                case ControllerType.UISinglePlayer:
                case ControllerType.UIMultiPlayer:
                default:
                    return new ConsoleSinglePlayerController();

            }

        }
    }
}
