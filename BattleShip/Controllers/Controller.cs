﻿using System;
using BattleShip.Core.Controllers.CommandHandlers;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Controllers
{

    /*
     * This class is the base controller of the game and new type of games should be inherited from this class.
     * The control will start by calling StartGame() function and will run the Play() function in each game cycle 
     * till the game became finished
     */
    public abstract class Controller
    {
        // Indicating game state
        public GameStatus GameStatus;
        protected CommandHandler CommandHandler;

        public abstract void ShowMenu();
        public abstract void Play();
        public abstract void EndGame();

        public bool IsGameFinished()
        {
            if (GameStatus == Enums.GameStatus.Finished)
                return true;
            else
                return false;
        }

        public bool IsGameStarted()
        {
            if (GameStatus == Enums.GameStatus.Started)
                return true;
            else
                return false;
        }
		
		// Start game -> After starting the game no more ships could be added.  
        public void StartGame()
        {

            ShowMenu();

            while (!IsGameFinished())
                  Play();

        }
    }
}
