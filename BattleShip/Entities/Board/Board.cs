﻿using System;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Entities.Board
{
    /* Board class could be implemented in several ways base on the time-space tradeoff.
     * This class contains the common functionality and features of different implementations.
     */
    public abstract class Board
    {
        public int RowNum { get; private set; }
        public int ColNum { get; private set; }

        protected Board(int rowNum, int colNum)
        {
            this.RowNum = rowNum;
            this.ColNum = colNum;
            CreateBoard();
        }

        public abstract void CreateBoard();
        public abstract void AddShip(Ship.Ship ship);
        public abstract bool IsShipPlaceMentPossible(Ship.Ship ship);
        public abstract bool GetCoordinateValidity(Coordinate coordinate);
        public abstract BoardCellStatus GetCellStatus(Coordinate coordinate);
        public abstract void UpdateCellStatus(Coordinate pos, BoardCellStatus status, int shipId);
        public abstract int GetShipId(Coordinate pos);
        public abstract void ShowBoard();
    }
}
