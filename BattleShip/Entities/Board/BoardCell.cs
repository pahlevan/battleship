﻿using System;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Entities.Board
{
    public class BoardCell
    {
        public BoardCellStatus Status { get; private set; }
        public int ShipId { get; private set; }

        public BoardCell()
        {
            Clear();
        }

        public void Clear()
        {
            Update(BoardCellStatus.Water,-1);
        }

        public void SetShipId(int id)
        {
            this.ShipId = id;
        }

        public void Update(BoardCellStatus status, int shipId)
        {
            this.Status = status;
            this.ShipId = shipId;
        }
    }
}
