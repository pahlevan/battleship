﻿using System;
using BattleShip.Core.Enums;

namespace BattleShip.Core.Entities.Board
{
    public class BoardCreator
    {

        public Board CreateBoard(BoardType boardType, int rowNum, int colNum)
        {

            switch (boardType)
            {
                case BoardType.Grid:
                    return new GridBoard(rowNum, colNum);
                default:
                    return new GridBoard(rowNum, colNum);


            }

        }

    }
}
