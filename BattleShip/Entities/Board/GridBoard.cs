﻿using System;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Entities.Board
{
    /* In GridBoard class, the board functionality is defined in terms of a 2D array
     */
    public class GridBoard : Board
    {
        public BoardCell[,] Board { get; private set; }

        public GridBoard(int RowNum, int ColNum) : base(RowNum, ColNum){}

        public override void CreateBoard()
        {
            Board = new BoardCell[RowNum, ColNum];

            for (int row = 0; row < RowNum; row++)
                for (int column = 0; column < ColNum; column++)
                    Board[row, column] = new BoardCell();

        }

        public override BoardCellStatus GetCellStatus(Coordinate coordinate)
        {
			if(!GetCoordinateValidity(coordinate))
				return BoardCellStatus.OutOfBound;
            return Board[coordinate.Row,coordinate.Col].Status;
        }

        public override void UpdateCellStatus(Coordinate coordinate, BoardCellStatus status, int shipId)
        {
			if(!GetCoordinateValidity(coordinate))
				return;
			Board[coordinate.Row, coordinate.Col].Update(status, shipId);

        }

        public override void AddShip(Ship.Ship ship)
        {
            Coordinate pos = new Coordinate(ship.HeadCoordinate.Row, ship.HeadCoordinate.Col);

            for (int i = 0; i < ship.TotalPieceNum; i++)
            {
                UpdateCellStatus(pos, BoardCellStatus.UnDamagedShipPiece, ship.Id);
                if (ship.Orientation == ShipOrientation.Vertical)
                    pos.IncreaseRow();
                else
                    pos.IncreaseCol();
            }

        }


        public override bool IsShipPlaceMentPossible(Ship.Ship ship)
        {
            Coordinate pos = new Coordinate(ship.HeadCoordinate.Row, ship.HeadCoordinate.Col);

            for (int i = 0; i < ship.TotalPieceNum; i++)
            {

                if (!GetCoordinateValidity(pos))
                {
                    return false;
                }

                if (GetCellStatus(pos) != BoardCellStatus.Water)
                {
                    return false;
                }

                if (ship.Orientation == ShipOrientation.Vertical)
                    pos.IncreaseRow();
                else
                    pos.IncreaseCol();
            }

            return true;
        }

        public override bool GetCoordinateValidity(Coordinate coordinate)
        {
            if (coordinate.Row >= RowNum || coordinate.Row < 0)
                return false;
            if (coordinate.Col >= ColNum || coordinate.Col < 0 )
                return false;

            return true;
        }

        public override int GetShipId(Coordinate coordinate)
        {
            if (!GetCoordinateValidity(coordinate))
                return -1;

            return Board[coordinate.Row, coordinate.Col].ShipId;
        }


        public override void ShowBoard()
        {
            for (int row = 0; row < RowNum; row++) { 
                for (int column = 0; column < ColNum; column++)
                {
                    if (Board[row, column].Status == BoardCellStatus.Water)
                        Console.Write("* ");
                    else if (Board[row, column].Status == BoardCellStatus.DamagedShipPiece)
                        Console.Write("# ");
                    else
                        Console.Write(Board[row, column].ShipId + " ");

                }
                Console.WriteLine();
        }
        }


    }
}
