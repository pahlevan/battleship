﻿using System;
using System.Collections.Generic;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Entities
{
    /*
     * Each player has a Board and a Ship list
     */
    public class Player
    {
        public PlayerStatus Status { get; private set; }
        public Board.Board Board { get; private set; }
        public List<Ship.Ship> Ships { get; private set; }
        private int healthyShipsNum;

        public Player(Board.Board board)
        {
            Status = PlayerStatus.Playing;
            this.Board = board;
            healthyShipsNum = 0;
            Ships = new List<Ship.Ship>();
        }

        public int GetNewShipId()
        {
            return Ships.Count;
        }

        public bool IsShipIdValid(int id)
        {
            if (id >= 0 && id < Ships.Count)
                return true;
            return false;
        }

        public CommandResult AddShip(Ship.Ship ship)
        {
            if (!Board.IsShipPlaceMentPossible(ship) || ship.TotalPieceNum == 0)
                return CommandResult.AddShipFaild;
            ship.SetId(GetNewShipId());
            Ships.Add(ship);
            Board.AddShip(ship);
            healthyShipsNum++;
            return CommandResult.AddShipDone;
        }


        public CommandResult UnderAttack(Coordinate coordinate)
        {
            if(!Board.GetCoordinateValidity(coordinate))
                return CommandResult.AttackFaild;

            BoardCellStatus cellStatus = Board.GetCellStatus(coordinate);
            if (cellStatus == BoardCellStatus.DamagedShipPiece)
                return CommandResult.AttackRepeated;
            if ( cellStatus!= BoardCellStatus.UnDamagedShipPiece)
                return CommandResult.AttackMiss;

            int shipId = Board.GetShipId(coordinate);

            if(!IsShipIdValid(shipId))
                return CommandResult.AttackFaild;

            Board.UpdateCellStatus(coordinate, BoardCellStatus.DamagedShipPiece, shipId);

            bool isSunk = Ships[shipId].Damage();
            if (isSunk)
            {
                healthyShipsNum--;
                UpdatePlayerStatus();
                return CommandResult.AttackSunk;
            }
            else
                return CommandResult.AttackHit;
        }

    
        public void UpdatePlayerStatus()
        {
            if (healthyShipsNum == 0 && Ships.Count != 0)
                Status = PlayerStatus.Lost;
        }

        public String GetStatusInfo()
        {
            String result = "--------\n";

            switch (Status)
            {
                case PlayerStatus.Lost:
                    result += "Player Lost!\n";
                    break;
                case PlayerStatus.Playing:
                    result += "The game is running!\n";
                    break;
                case PlayerStatus.Won:
                    result += "Player Won!\n";
                    break;
            }

            result += "#Alive Ships: " + healthyShipsNum + ", #Sunk Ships: " + (Ships.Count - healthyShipsNum);
            result += "\n--------";

            return result;

        }

    }
}
