﻿using System;
using BattleShip.Core.Entities.Board;
using BattleShip.Core.Enums;
using BattleShip.Core.Utils;

namespace BattleShip.Core.Entities.Ship
{
    public class Ship
    {
        public int Id { get; private set; }
        public int TotalPieceNum { get; private set; }
        public int Health { get; private set; }
        public Coordinate HeadCoordinate { get; private set; }
        public ShipOrientation Orientation { get; private set; }


        public Ship(int id, Coordinate headCoordinate, ShipOrientation orientation, int totalPieceNum)
        {
            this.Id = id;
            this.HeadCoordinate = headCoordinate;
            this.Orientation = orientation;
            this.TotalPieceNum = totalPieceNum;
            this.Health = totalPieceNum;
        }


        public Boolean Damage()
        {
            Health--;
            return IsSunk();
        }

        public Boolean IsSunk()
        {
            return Health == 0 ? true : false;
        }

        internal void SetId(int id)
        {
            this.Id = id;
        }
    }
}
