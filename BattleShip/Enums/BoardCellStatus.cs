﻿using System;
namespace BattleShip.Core.Enums
{
    public enum BoardCellStatus
    {
        Water, UnDamagedShipPiece, DamagedShipPiece, OutOfBound
    }
}
