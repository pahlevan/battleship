﻿using System;
namespace BattleShip.Core.Enums
{
    public enum CommandResult
    {
        AttackHit, AttackMiss, AttackSunk, AttackFaild, AttackRepeated, AddShipDone, AddShipFaild, None
    }
}
