﻿using System;
namespace BattleShip.Core.Enums
{
    public enum CommandType
    {
        AddShip, Attack, Start, Menu, Quit, Status, Invalid
    }
}
