﻿namespace BattleShip.Core.Enums
{
    public enum ControllerType
    {
        ConsoleSinglePlayer, ConsoleMultiPlayer,UISinglePlayer, UIMultiPlayer
    }
}
