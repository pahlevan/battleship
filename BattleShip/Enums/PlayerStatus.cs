﻿using System;
namespace BattleShip.Core.Enums
{
    public enum PlayerStatus
    {
        Playing, Won, Lost,
    }
}
