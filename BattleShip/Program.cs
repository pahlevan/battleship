﻿using System;
using BattleShip.Core.Controllers;
using BattleShip.Core.Enums;
namespace BattleShip
{
    class Program
    {
        static void Main(string[] args)
        {
            // create a contoller using controllerCreator class and start the game
            ContollerCreator contollerCreator = new ContollerCreator();
            Controller controller = contollerCreator.CreateController(ControllerType.ConsoleSinglePlayer);
            controller.StartGame();
        }
    }
}
