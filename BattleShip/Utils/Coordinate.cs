﻿using System;
namespace BattleShip.Core.Utils
{
    public class Coordinate
    {
        public int Row { get; set; }
        public int Col { get; set; }

        public Coordinate(int row, int col)
        {
            this.Row = row;
            this.Col = col;
        }

        public void IncreaseRow()
        {
            Row++;
        }

        public void IncreaseCol()
        {
            Col++;
        }

    }
}
