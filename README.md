# Battle Ship State Tracker
Single player (1 board, no enemy) battle ship game with state tracker console application in .NET Core 2.1.

## The task 
The task is to implement a Battleship state-tracker for a single player that must support the following logic: 

* Create a board 
* Add a battleship to the board 
* Take an “attack” at a given position, and report back whether the attack resulted in a hit or a miss 
* Return whether the player has lost the game yet (i.e. all battleships are sunk) 

### Background 

This exercise is based on the classic game “Battleship”. 

* Two players 
* Each player has a 10x10 board 
* During setup, players can place an arbitrary number of “battleships” on their board. The ships are 1-by-n sized, must fit entirely on the board, and must be aligned either vertically or horizontally. 
* During play, players take a turn “attacking” a single position on the opponent’s board, and the opponent must respond by either reporting a “hit” on one of their battleships (if one occupies that position) or a “miss” 
* A battleship is sunk if it has been hit on all the squares it occupies 
* A player wins if all of their opponent’s battleships have been sunk.


## How To Run
The code is written in C# by using Visual Studio 2019 on macOS Mojave 10.14.6  and tested by Visual Studio 2019 on Windows 10.

### Dependencies
* [.NET Core 3.1](https://www.microsoft.com/net/download)


#### 1. Run the project
Run the project from visual studio and follow the instruction on the promt, or see below

#### 2. Instructions

Once the console applicatin is running, the user must type commands follow by the Enter key. See available commands listed below:
	
* '**/menu**' : Provide the list of all commands
* '**/quit**' : Exit the console application
* '**/status**' : Give the current status of the game (thenumber of alive ships and sunk ships)
* '**/attack row column**' Attack at the specified coordinates (with row(int) and y(int))
* '**/addship row column orientation size**' : Add a ship on the board, specifying its coordinates (with row(int) and y(int)), orientation ('v' for 'vertical' or 'h' for 'hozirontal'), and its size(int)


## File Structure

The Solution contains two projects(Battleship.Core & BattleShip.Test). 
BattleShip.Core contains:  
controllers -> Manages the game using 
Entities -> Ship, Board, and Player are the main entities  
Enums -> Different state/satus used in classes  
Utils -> Coordinates (here we used a 2D coordinate but more dimentions could be added)
Program.cs -> Main function which is the starting point of application where the contoller is being created.

## Main flow

- ContollerCreator class creates a controller based on the recieved controller type (e.g. ControllerType.ConsoleSinglePlayer).
- The game will be started by calling the controller.StartGame()
- StartGame() calls the play() which executes the users' commands and controles the game states.
- The Controller class contains a CommandHandler object which would be instantiated in the concerete controller class, in our case ConsoleSinglePlayerController.
- ConsoleSinglePlayeController serves only one player object via ConsoleCommandhandler as a commiunication channel.
- CommanHandler has a list of Command objects which are 

